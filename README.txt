
jsTree Documents module
-----------------
by Iván Campaña, ivan.campana@gmail.com

This module uses the jquery plugin jstree to show the document hierarchy and a list of the available documents,
it was originally intended for use in an intranet site, but can be used anywhere to give easy access to documents
either for a company or a public information site.
