(function ($) {
    Drupal.behaviors.jstree_documents = {
        attach:function (context, settings) {
		$("#jstree_documents_container").jstree({
			    "themes":{
				"theme": 'classic',
				//"url": settings.basePath + '/sites/all/libraries/jstree/',
				"dots":true,
				"icons":true
			    },
			    //set the cookie to be global
			    'json_data' : {
				   "ajax" : { "url" : settings.basePath + "jstree_documents/ajax/get_data" }
			    },
			    "cookies" : {
			      "cookie_options": { path: '/' }
			    },
			    "progressive_render" : true,
			    "plugins":[ "themes", "json_data", "cookies" , 'ui']
		}).bind("select_node.jstree", function (e, data) {
		        var current_node = data.rslt.obj;
		        if(current_node.data('url') != undefined){
				window.location = current_node.data('url');
			}
		});
        }
    };
}(jQuery));